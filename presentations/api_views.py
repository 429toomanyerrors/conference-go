from django.http import JsonResponse

from .models import Presentation


def api_list_presentations(request, conference_id):
    """
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.

    {
        "presentations": [
            {
                "title": presentation's title,
                "status": presentation's status name
                "href": URL to the presentation,
            },
            ...
        ]
    }
    """
    presentations = [
        {
            "title": p.title,
            "status": p.status.name,
            "href": p.get_api_url(),
        }
        for p in Presentation.objects.filter(conference=conference_id)
    ]
    return JsonResponse({"presentations": presentations})


def api_show_presentation(request, pk):

    # Returns the details for the Presentation model specified
    # by the pk parameter.

    # This should return a dictionary with the presenter's name,
    # their company name, the presenter's email, the title of
    # the presentation, the synopsis of the presentation, when
    # the presentation record was created, its status name, and
    # a dictionary that has the conference name and its URL
    presenter = Presentation.objects.get(id=pk)
    return JsonResponse(
        {
            "presenter_name": presenter.presenter_name,
            "company_name": presenter.company_name,
            "presenter_email": presenter.presenter_email,
            "title": presenter.title,
            "synopsis": presenter.synopsis,
            "created": presenter.created,
            "status": presenter.status,
            "conference": {
                "name": presenter.conference.name,
                "href": presenter.conference.get_api_url(),
            },
        }
    )
